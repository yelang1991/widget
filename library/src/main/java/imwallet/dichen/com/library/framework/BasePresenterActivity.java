package imwallet.dichen.com.library.framework;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * 和Presenter绑定的Activity基类
 *
 * @author Administrator
 * @date 2018/6/4
 */

public abstract class BasePresenterActivity<P extends BasePresenter<V>, V extends BaseView> extends BaseActivity {
    protected P mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = initPresenter();
        mPresenter.onCreate();
    }

    /**
     * 初始化Presenter
     *
     * @return
     */
    public abstract P initPresenter();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestory();
        mPresenter = null;
    }
}
