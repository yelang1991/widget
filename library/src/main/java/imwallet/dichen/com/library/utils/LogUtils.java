package imwallet.dichen.com.library.utils;

import android.util.Log;

import imwallet.dichen.com.library.app.App;

/**
 *
 * @author Administrator
 * @date 2018/6/5
 */

public class LogUtils {

    public static void e(String TAG, String msg) {
        if(App.get().isDebug()) {
            Log.e(TAG, msg);
        }
    }
}
