package imwallet.dichen.com.library.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 控制viewpager的左右滑动
 *
 * @author Administrator
 * @date 2018/6/12
 */

public class SlideViewPager extends ViewPager {
    /**
     * 是否支持滑动
     */
    private boolean mIsSlide = false;

    public SlideViewPager(@NonNull Context context) {
        super(context);
    }

    public SlideViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mIsSlide;
    }

    public void setSlide(boolean isSlide) {
        mIsSlide = isSlide;
    }
}
