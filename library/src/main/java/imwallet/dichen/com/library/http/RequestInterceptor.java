package imwallet.dichen.com.library.http;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import imwallet.dichen.com.library.app.App;
import imwallet.dichen.com.library.bean.User;
import imwallet.dichen.com.library.constant.Constant;
import imwallet.dichen.com.library.utils.ACache;
import imwallet.dichen.com.library.utils.LogUtils;
import imwallet.dichen.com.library.utils.Tools;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * 请求拦截器
 *
 * @author Administrator
 * @date 2018/7/10
 */

public class RequestInterceptor implements Interceptor {

    /**
     * 添加统一的header
     *
     * @param chain
     * @return
     * @throws IOException
     */
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder builder = request.newBuilder();
        Headers.Builder header = new Headers.Builder();
        header.add("Content-type:application/json;charset=UTF-8");
        User user = (User) ACache.get(App.get()).getAsObject(Constant.Cache.KEY_USER);
        if(user != null && !TextUtils.isEmpty(user.getAccessToken())) {
            header.add("access-token", user.getAccessToken());
        }
        String language = "zh-CN";
        if(!TextUtils.isEmpty(Tools.getLanguage())) {
            language = Tools.getLanguage();
        }
        header.add("locale", language);
        builder.headers(header.build());
        Request newRequest = builder.build();
        // 日志打印相关
        RequestBody requestBody = request.body();
        String body = null;
        if(requestBody != null) {
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            Charset charset = Charset.forName("UTF-8");
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(charset);
            }
            body = buffer.readString(charset);
        }
        Response response = chain.proceed(newRequest);
        ResponseBody responseBody = response.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();
        Charset charset = Charset.forName("UTF-8");
        MediaType contentType = responseBody.contentType();
        if (contentType != null) {
            try {
                charset = contentType.charset(charset);
            } catch (UnsupportedCharsetException e) {
                e.printStackTrace();
            }
        }
        String rBody = buffer.clone().readString(charset);
        LogUtils.e("Response", String.format("请求url：%s\n请求body：%s\n响应body：%s",response.request().url(), body, rBody));
        return response;
    }
}
