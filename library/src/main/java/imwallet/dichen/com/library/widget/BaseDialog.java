package imwallet.dichen.com.library.widget;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.View;
import android.view.Window;

/**
 *
 * @author Administrator
 * @date 2018/5/30
 */

public abstract class BaseDialog extends Dialog {

    public BaseDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public BaseDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     * 设置提示正文
     *
     * @param msg 提示正文
     */
    public abstract BaseDialog setMessage(String msg);

    /**
     * 设置点击事件
     *
     * @param clickListener 按钮点击事件
     */
    public void setOnClickListener(OnClickListener clickListener) {

    }

    public interface OnClickListener {
        /**
         * 点击确定
         *
         * @param view
         */
        void onCancelClick(View view);

        /**
         * 点击取消
         *
         * @param view
         */
        void onConfirmClick(View view);
    }
}
