package imwallet.dichen.com.library.framework;

/**
 * 监听Android生命周期
 *
 * @author Administrator
 * @date 2018/6/4
 */

public interface Lifecycle {
    /**
     * activity创建时
     *
     * @param tag
     */
    void onCreate(String tag);

    /**
     * activity创建成功后
     *
     * @param tag
     */
    void onActivityCreated(String tag);

    /**
     * 页面获取焦点时
     *
     * @param tag
     */
    void onResume(String tag);

    /**
     * 页面不可见时
     *
     * @param tag
     */
    void onPause(String tag);

    /**
     * activity销毁时
     *
     * @param tag
     */
    void onDestory(String tag);

    /**
     * view销毁时
     *
     * @param tag
     */
    void onDestoryView(String tag);
}
