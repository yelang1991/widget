package imwallet.dichen.com.library.http;

import imwallet.dichen.com.library.app.App;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public class ApiManager {
    private static ApiManager sInstance;
    private Retrofit mRetrofit;

    private ApiManager() {
        String baseUrl;
        if(App.get().isDebug()) {
            baseUrl = "http://120.79.230.183/wallet-front/";
        } else {
            baseUrl = "";
        }
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new RequestInterceptor())
                .build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    public static ApiManager get() {
        if(sInstance == null) {
            synchronized (ApiManager.class) {
                if(sInstance == null) {
                    sInstance = new ApiManager();
                }
            }
        }
        return sInstance;
    }

    public <T> T createService(Class<T> clazz) {
        return mRetrofit.create(clazz);
    }
}
