package imwallet.dichen.com.library.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 类名：  DateUtil.java
 */

public class DateUtil {

    public final static String FORMAT_YEAR = "yyyy";
    public final static String FORMAT_YEAR_MONTH_DAY = "yyyy年MM月dd日";
    public final static String FORMAT_MONTH_DAY = "MM月dd日";

    public final static String FORMAT_ONLY_MONTH_DAY = "M月dd日";

    public final static String FORMAT_DATE = "yyyy-MM-dd";

    public final static String FORMAT_TIME = "HH:mm";
    public final static String FORMAT_TIME_ss = "HH:mm:ss";

    public final static String FORMAT_MONTH_DAY_TIME = "MM月dd日  hh:mm";
    public final static String FORMAT_DATE_TIME_car = "yyyy-MM-ddHH:mm:ss";
    public final static String FORMAT_DATE_TIME_air = "yyyy-MM-dd HH:mm";
    public final static String FORMAT_DATE_TIME_order = "MM-dd HH:mm";
    public final static String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_DATE_TIME_ms = "yyyy-MM-dd HH:mm";

    public final static String FORMAT_DATE1_TIME = "yyyy/MM/dd HH:mm";

    public final static String FORMAT_DATE_TIME_SECOND = "yyyy/MM/dd HH:mm:ss";

    public final static String FORMAT_MONTH_DAY_LINE = "MM-dd";


    public static String setCarDate(String formatType) {
        long timelong = DateUtil.stringToLong(formatType, DateUtil.FORMAT_DATE_TIME_car);
        String time = DateUtil.longToString(timelong, DateUtil.FORMAT_DATE_TIME_order);
        return time;
    }

    public static String setAirDate(String formatType) {
        long timelong = DateUtil.stringToLong(formatType, DateUtil.FORMAT_DATE_TIME_air);
        String time = DateUtil.longToString(timelong, DateUtil.FORMAT_DATE_TIME_order);
        return time;
    }

    /**
     * date类型转换为String类型
     * formatType格式为yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日 HH时mm分ss秒
     * data Date类型的时间
     *
     * @param data
     * @param formatType
     * @return
     */

    public static String dateToString(Date data, String formatType) {

        return new SimpleDateFormat(formatType).format(data);
    }

    /**
     * long类型转换为String类型
     * currentTime要转换的long类型的时间
     * formatType要转换的string类型的时间格式
     *
     * @param currentTime
     * @param formatType
     * @return
     */

    public static String longToString(long currentTime, String formatType) {

        String strTime;
        // long类型转成Date类型
        Date date = longToDate(currentTime, formatType);
        // date类型转成String
        strTime = dateToString(date, formatType);
        return strTime;
    }


    /**
     * string类型转换为date类型
     * strTime要转换的string类型的时间，formatType要转换的格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日
     * HH时mm分ss秒，
     * strTime的时间格式必须要与formatType的时间格式相同
     *
     * @param strTime
     * @param formatType
     * @return
     */

    public static Date stringToDate(String strTime, String formatType) {

        SimpleDateFormat formatter = new SimpleDateFormat(formatType);
        Date date = null;
        try {
            date = formatter.parse(strTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    /**
     * long转换为Date类型
     * currentTime要转换的long类型的时间
     * formatType要转换的时间格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日 HH时mm分ss秒
     *
     * @param currentTime
     * @param formatType
     * @return
     */

    public static Date longToDate(long currentTime, String formatType) {

        // 根据long类型的毫秒数生命一个date类型的时间
        Date dateOld = new Date(currentTime);
        // 把date类型的时间转换为string
        String sDateTime = dateToString(dateOld, formatType);
        // 把String类型转换为Date类型
        Date date = stringToDate(sDateTime, formatType);
        return date;
    }

    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     *
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     * @return String 返回值为：xx天xx小时xx分xx秒
     */
    public static String getDistanceTime(String str1, String str2) {
        DateFormat df = new SimpleDateFormat(FORMAT_DATE_TIME_ms);
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff;
            if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        if (day != 0) {
            sb.append(day + "天");
        }
        if (hour != 0) {
            sb.append(hour + "小时");
        }
        if (min != 0) {
            sb.append(min + "分");
        }
        if (sec != 0) {
            sb.append(sec + "秒");
        }
        return sb.toString();
    }


    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     *
     * @param time1 时间参数 1 格式：1990-01-01 12:00:00
     * @param time2 时间参数 2 格式：2009-01-01 12:00:00
     * @return String 返回值为：xx天xx小时xx分xx秒
     */
    public static String getDistanceLongTime(long time1, long time2) {
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        long diff;
        if (time1 < time2) {
            diff = time2 - time1;
        } else {
            diff = time1 - time2;
        }
        day = diff / (24 * 60 * 60 * 1000);
        hour = (diff / (60 * 60 * 1000) - day * 24);
        min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
        sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        StringBuilder sb = new StringBuilder();
        if (day != 0) {
            sb.append(day + "天");
        }
        if (hour != 0) {
            sb.append(hour + "小时");
        }
        if (min != 0) {
            sb.append(min + "分");
        }
        if (sec != 0) {
            sb.append(sec + "秒");
        }
        return sb.toString();
    }

    /**
     * string类型转换为long类型
     * strTime要转换的String类型的时间
     * formatType时间格式
     * strTime的时间格式和formatType的时间格式必须相同
     *
     * @param strTime
     * @param formatType
     * @return
     */

    public static long stringToLong(String strTime, String formatType) {

        // String类型转成date类型
        Date date = stringToDate(strTime, formatType);
        if (date == null) {
            return 0;
        } else {
            // date类型转成long类型
            return dateToLong(date);
        }
    }

    /**
     * date类型转换为long类型
     * date要转换的date类型的时间
     *
     * @param date
     * @return
     */

    public static long dateToLong(Date date) {

        return date.getTime();
    }

    /**
     * 判断是否为今天(效率比较高)
     *
     * @param day 传入的 时间  "2016-06-28 10:10:30" "2016-06-28" 都可以
     * @return true今天 false不是
     * @throws ParseException
     */
    public static boolean IsToday(String day) throws ParseException {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);

        Calendar cal = Calendar.getInstance();
        Date date = getDateFormat().parse(day);
        cal.setTime(date);

        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            if (diffDay == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否为昨天(效率比较高)
     *
     * @param day 传入的 时间  "2016-06-28 10:10:30" "2016-06-28" 都可以
     * @return true今天 false不是
     * @throws ParseException
     */
    public static boolean IsYesterday(String day) throws ParseException {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);

        Calendar cal = Calendar.getInstance();
        Date date = getDateFormat().parse(day);
        cal.setTime(date);

        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            if (diffDay == -1) {
                return true;
            }
        }
        return false;
    }

    public static String IsCureentTodayorYesterdayorWeek(String time) {
        String Week = "今天";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_TIME_ms);
            Date dt1 = sdf.parse(time);
            if (dt1.getTime() >= System.currentTimeMillis()) {
                if (IsToday(time)) {
                    return "今天";
                } else if (IsYesterday(time)) {
                    return "明天";
                } else {
                    Week = getWeek(time);
                    return Week;
                }
            } else {
                return "";
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "今天";
        }
    }

    public static String IsTodayorYesterdayorWeek(String time) {
        String Week = "今天";
        try {
            if (IsToday(time)) {
                return "今天";
            } else if (IsYesterday(time)) {
                return "明天";
            } else {
                Week = getWeek(time);
                return Week;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "今天";
        }
    }


    private static String getWeek(String time) {
        String Week = "周";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//也可将此值当参数传进来
//        Date  curDate = new Date(System.currentTimeMillis());
//        String time = format.format(curDate);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(format.parse(time));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                Week += "日";
                break;
            case 2:
                Week += "一";
                break;
            case 3:
                Week += "二";
                break;
            case 4:
                Week += "三";
                break;
            case 5:
                Week += "四";
                break;
            case 6:
                Week += "五";
                break;
            case 7:
                Week += "六";
                break;
            default:
                break;
        }
        return Week;
    }

    public static SimpleDateFormat getDateFormat() {
        if (null == DateLocal.get()) {
            DateLocal.set(new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA));
        }
        return DateLocal.get();
    }

    /**
     * 获取两个时间的天数差
     *
     * @param starTime 开始时间
     * @param endTime  结束时间
     * @return 相差天数
     */
    public static long getDateCount(long starTime, long endTime) {
        return (starTime - endTime) / (1000 * 60 * 60 * 24);
    }

    public static Date getTomorrow(Date today) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        return calendar.getTime();
    }

    private final static ThreadLocal<SimpleDateFormat> dateFormater = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    private final static ThreadLocal<SimpleDateFormat> dateFormater2 = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
    private static ThreadLocal<SimpleDateFormat> DateLocal = new ThreadLocal<SimpleDateFormat>();

    public static ArrayList<Date> getDates(long statTime, long endTime) {
        ArrayList<Date> dates = null;
        if (statTime > -1 && endTime > -1) {
            Date startDate = new Date(statTime);
            Date endDate = new Date(endTime);
            dates = new ArrayList<>();
            dates.add(startDate);
            dates.add(endDate);
        }
        return dates;
    }
}
