package imwallet.dichen.com.library.app;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

/**
 *
 * @author Administrator
 * @date 2018/6/7
 */

public class App extends Application {
    protected static App sApp;
    protected boolean sDebug;
    private RefWatcher mRefWatcher;

//    public static RefWatcher getRefWatcher(Context context) {
//        App application = (App) context.getApplicationContext();
//        return application.mRefWatcher;
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApp = this;
        initSdk();
    }

    /**
     * 初始化第三方sdk
     */
    protected void initSdk() {
        initBugly();
        initArouter();
        initLeakCanary();
    }

    private void initLeakCanary() {
        mRefWatcher = LeakCanary.install(this);
    }

    private void initBugly() {
//        if(isDebug()) {
//            CrashReport.initCrashReport(getApplicationContext(), "7a9366e069", true);
//        } else {
//            CrashReport.initCrashReport(getApplicationContext(), "7c9f422e41", false);
//        }
    }

    private void initArouter() {
//        // 这两行必须写在init之前，否则这些配置在init过程中将无效
//        if (isDebug()) {
//            // 打印日志
//            ARouter.openLog();
//            // 开启调试模式
//            ARouter.openDebug();
//        }
//        // 尽可能早，推荐在Application中初始化
//        ARouter.init(sApp);
    }

    public static App get() {
        return sApp;
    }

    public boolean isDebug() {
        return sDebug;
    }

    public void setDebug(boolean isDebug) {
        sDebug = isDebug;
    }

}
