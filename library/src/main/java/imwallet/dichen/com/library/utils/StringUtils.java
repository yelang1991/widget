package imwallet.dichen.com.library.utils;

import android.text.TextUtils;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2018/5/18.
 */

public class StringUtils {

    /**
     * 格式化货币，保留2位小数
     *
     * @return
     */
    public static String formtCurrency(String data) {
        if(TextUtils.isEmpty(data)) {
            return "";
        }
        return new BigDecimal(data).setScale(2, BigDecimal.ROUND_DOWN).toPlainString();
    }


    public static String settingStar(String phone) {
        String phoneNumber = phone.substring(0, 8) + "..." + phone.substring(34, phone.length());
        return phoneNumber;
    }

    public static String settingStar2(String phone) {
        String phoneNumber = phone.substring(0, 12) + "..." + phone.substring(54, phone.length());
        return phoneNumber;
    }
}
