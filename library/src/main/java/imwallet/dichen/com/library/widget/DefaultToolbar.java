package imwallet.dichen.com.library.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import imwallet.dichen.com.library.R;
import imwallet.dichen.com.library.utils.DensityUtil;

/**
 * 默认toolbar
 *
 * @author Administrator
 * @date 2018/5/25
 */

public class DefaultToolbar extends Toolbar {
    private ImageView mNavigationImage;
    private TextView mTitleView;
    private TextView mMenuView;

    public DefaultToolbar(Context context) {
        super(context);
        initView();
    }

    public DefaultToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,
                DensityUtil.dip2px(getContext(), 70));
        setLayoutParams(layoutParams);
        // 去掉默认间隔
        setContentInsetsAbsolute(0, 0);
        setPadding(0, getStatusBarHeight(getContext()), 0, 0);
        mNavigationImage = new ImageView(getContext());
        LayoutParams imageParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        setBackgroundColor(Color.parseColor("#20CE88"));
        imageParams.gravity = Gravity.CENTER_VERTICAL;
        mNavigationImage.setLayoutParams(imageParams);
        mNavigationImage.setPadding(DensityUtil.dip2px(getContext(), 10),
                DensityUtil.dip2px(getContext(), 10),
                DensityUtil.dip2px(getContext(), 10),
                DensityUtil.dip2px(getContext(), 10));
        mNavigationImage.setImageResource(R.mipmap.icon_back);
        addView(mNavigationImage);
    }

    /**
     * 获取状态栏高度
     *
     * @param context
     * @return
     */
    private int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void setTitle(String title) {
        if(mTitleView == null) {
            mTitleView = new TextView(getContext());
            LayoutParams titleParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            titleParams.gravity = Gravity.CENTER;
            mTitleView.setLayoutParams(titleParams);
            mTitleView.setTextColor(Color.parseColor("#0E1D46"));
            mTitleView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            addView(mTitleView);
        }
        if(!TextUtils.isEmpty(title)) {
            mTitleView.setText(title);
        }
    }

    @Override
    public void setNavigationIcon(@DrawableRes int resId) {
        if(resId > 0) {
            mNavigationImage.setImageResource(resId);
        }
    }

    @Override
    public void setNavigationOnClickListener(OnClickListener listener) {
        if(mNavigationImage != null) {
            mNavigationImage.setOnClickListener(listener);
        }
    }

    @Override
    public void inflateMenu(@MenuRes int resId) {
        if(resId > 0) {
            if(mMenuView != null) {
                removeView(mMenuView);
            }
            getMenu().clear();
            super.inflateMenu(resId);
            Menu menu = getMenu();
            if(menu != null && menu.size() > 0) {
                for(int i = 0; i < menu.size(); i ++) {
                    MenuItem item = menu.getItem(i);
                    if(item != null) {
                        SpannableString title = new SpannableString(item.getTitle());
                        title.setSpan(new ForegroundColorSpan(Color.parseColor("#0E1D46")),
                                0, title.length(), 0);
                        item.setTitle(title);
                    }
                }
            }
        }
    }

    public void setMenuText(String text) {
        if(!TextUtils.isEmpty(text)) {
            initMenuView();
            mMenuView.setText(text);
        }
    }

    public void setMenuImage(@DrawableRes int resId) {
        if(resId > 0) {
            initMenuView();
            Drawable drawable = getResources().getDrawable(resId);
            if(drawable != null) {
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                mMenuView.setCompoundDrawables(drawable, null, null, null);
            }
        }
    }

    private void initMenuView() {
        if(mMenuView == null) {
            mMenuView = new TextView(getContext());
            LayoutParams menuParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            menuParams.gravity = Gravity.END | Gravity.CENTER_VERTICAL;
            mMenuView.setLayoutParams(menuParams);
            mMenuView.setTextColor(Color.parseColor("#0E1D46"));
            mMenuView.setPadding(DensityUtil.dip2px(getContext(), 10),
                    DensityUtil.dip2px(getContext(), 10),
                    DensityUtil.dip2px(getContext(), 10),
                    DensityUtil.dip2px(getContext(), 10));
            mMenuView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            addView(mMenuView);
        }
    }

    public ImageView getNavigationImage() {
        return mNavigationImage;
    }

    public TextView getMenuView() {
        return mMenuView;
    }

    public TextView getTitleView() {
        return mTitleView;
    }
}
