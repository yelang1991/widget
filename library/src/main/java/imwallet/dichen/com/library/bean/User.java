package imwallet.dichen.com.library.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * @author Administrator
 */
public class User implements Parcelable,Serializable {
    private String id;
    private String mobile;
    private String referrerCode ;
    private String referrerMobile  ;
    public User() {

    }

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public String getReferrerMobile() {
        return referrerMobile;
    }

    public void setReferrerMobile(String referrerMobile) {
        this.referrerMobile = referrerMobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String accessToken;

    /**
     * 读取的顺序必须和写入的顺序保持一致，
     * 否则将会导致异常
     *
     * @param in
     */
    public User(Parcel in) {
        id = in.readString();
        mobile = in.readString();
        accessToken = in.readString();
        referrerCode = in.readString();
        referrerMobile = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(mobile);
        dest.writeString(accessToken);
        dest.writeString(referrerCode);
        dest.writeString(referrerMobile);
    }
}
