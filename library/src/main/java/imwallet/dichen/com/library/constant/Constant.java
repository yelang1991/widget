package imwallet.dichen.com.library.constant;

import android.os.Environment;

/**
 * 常量保存
 *
 * @author Administrator
 * @date 2018/5/15
 */

public interface Constant {
    /**
     * 缓存
     */
    class Cache {
        public static String KEY_USER = "login_user";
        /**
         * 是否创建过钱包
         */
        public static String KEY_GUIDE = "key_guide";
        /**
         * 登录账号
         */
        public static String KEY_PHONE = "key_phone";

        public static String KEY_PATH = "key_path";
    }

    /**
     * eventbus code
     */
    class Code {
        /**
         * 更新钱包资产
         */
        public static int CODE_UPDATE_ASSETS = 0x100;
        /**
         * 添加币到钱包
         */
        public static int CODE_ADD_ASSETS = 0x101;

        /**
         * 更新交易记录
         */
        public static int CODE_ADD_RECORD = 0x103;
        /**
         * 更新行情
         */
        public static int CODE_UPDATE_MARK = 0x105;
        /**
         * 关闭页面
         */
        public static int CODE_FINISH_RISK = 0x106;
        /**
         * 刷新钱包
         */
        public static int CODE_UPDATE_WALLET = 0x107;
    }

    public static class PermissionCode {
        /**
         * 读写sd卡
         */
        public static int CODE_READ_EXTERNAL_STORAGE = 0X001;
    }

    class Key {
        /**
         * 钱包id
         */
        public static String KEY_WALLETID = "key_walletId";
    }

    class Path {
        /**
         * apk目录
         *
         * @return
         */
        public static String getApkPath() {
            return Environment.getExternalStorageDirectory() + "/imWallet/apk";
        }
    }
}
