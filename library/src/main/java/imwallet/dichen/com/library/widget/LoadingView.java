package imwallet.dichen.com.library.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import imwallet.dichen.com.library.R;

/**
 * 首次加载view
 *
 * @author Administrator
 * @date 2018/6/5
 */

public class LoadingView extends LinearLayout {
    public LoadingView(@NonNull Context context) {
        this(context, null);
    }

    public LoadingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER);
        setBackgroundColor(Color.parseColor("#fcfcfc"));
        MarginLayoutParams layoutparams = new MarginLayoutParams(MarginLayoutParams.MATCH_PARENT,
                MarginLayoutParams.MATCH_PARENT);
        setLayoutParams(layoutparams);
        LayoutInflater.from(context).inflate(R.layout.view_loading, this, true);
    }

}
