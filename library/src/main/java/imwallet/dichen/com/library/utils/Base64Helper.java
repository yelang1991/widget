package imwallet.dichen.com.library.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 *
 * @author Administrator
 * @date 2018/6/12
 */

public class Base64Helper {

    /**
     * base64字符串转化成图片
     *
     * @param imgURL
     * @return
     */
    public static Bitmap generateImage(String imgURL)
    {
        //将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray = Base64.decode(imgURL.split(",")[1], Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * base64加密
     *
     * @param data
     * @return
     */
    public static String encryption(String data) {
        try {
            return Base64.encodeToString(data.getBytes(), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * base64解密
     *
     * @param data
     * @return
     */
    public static String decryption(String data) {
        try {
            return new String(Base64.decode(data.getBytes(), Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
