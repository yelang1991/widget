package imwallet.dichen.com.library.http;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public class Response<T> {
    private int code;
    private T result;
    private String message;
    /**
     * 请求成功
     */
    public static int RESULT_OK = 1;
    /**
     * 未登录
     */
    public static int CODE_NOLOGIN = 401;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
