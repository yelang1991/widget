package imwallet.dichen.com.library.widget;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import imwallet.dichen.com.library.R;

/**
 * 拍照、相册弹窗
 *
 * @author Administrator
 * @date 2018/6/11
 */

public class PictureDialog extends Dialog {
    private TextView mCameraTxt;
    private TextView mCancelTxt;
    private TextView mGalleryTxt;
    private OnSelectListener mOnSelectListener;

    public PictureDialog(@NonNull Context context) {
        this(context, 0);
    }

    public PictureDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.dialog_select_photo);
        mCameraTxt = (TextView) findViewById(R.id.photo_camera_tv);
        mGalleryTxt = (TextView) findViewById(R.id.photo_album_tv);
        mCancelTxt = (TextView) findViewById(R.id.photo_cancel_tv);
        Window window = getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.BOTTOM;
        window.setAttributes(layoutParams);
        mCameraTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnSelectListener != null) {
                    mOnSelectListener.onCameraClick();
                }
            }
        });
        mGalleryTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnSelectListener != null) {
                    mOnSelectListener.onGalleryClick();
                }
            }
        });
        mCancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setOnSelectListener(OnSelectListener onSelectListener) {
        mOnSelectListener = onSelectListener;
    }

    public interface OnSelectListener {
        /**
         * 点击拍照
         */
        void onCameraClick();

        /**
         * 点击相册
         */
        void onGalleryClick();
    }
}
