package imwallet.dichen.com.library.http;

/**
 * Http返回码
 *
 * @author Administrator
 * @date 2018/6/7
 */

public class HttpCode {
    //请求成功
    public final static int STATUS_OK = 200;
    //未授权
    public final static int  UNAUTHORIZED = 401;
    //服务器拒绝访问
    public final static int FORBIDDEN = 403;
    //找不到请求的地址
    public final static int NOT_FOUND = 404 ;
    //请求超时
    public final static int REQUEST_TIMEOUT = 408;
    //服务器内部错误
    public final static int INTERNAL_SERVER_ERROR = 500;
    //错误网关
    public final static int BAD_GATEWAY = 502;
    //服务不可用
    public final static int SERVICE_UNAVAILABLE = 503;
    //网关超时
    public final static int GATEWAY_TIMEOUT = 504;
    //未知错误
    public final static int UNKNOWN = -1;
    //DNS错误
    public final static int DNS_ERROR = -2;
    //网络连接失败
    public final static int NETWORK_NOT_CONNECTED = -3;
    //暂无分类
    public final static int NOT_CATEGORY_CODE = 10001;
}
