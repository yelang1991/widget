package imwallet.dichen.com.library.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import imwallet.dichen.com.library.R;

/**
 * 下载弹窗
 *
 * @author Administrator
 * @date 2018/6/23
 */

public class DownloadDialog extends BaseDialog {
    private TextView mCancelTxt;
    private TextView mInstallTxt;
    private TextView mProgressTxt;
    private ProgressBar mProgressBar;
    private OnLostener mListener;

    public DownloadDialog(@NonNull Context context) {
        this(context, 0);
    }

    public DownloadDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.dialog_download);
        init();
    }

    private void init() {
        getWindow().setBackgroundDrawableResource(R.drawable.dialog_default_shape);
        mCancelTxt = (TextView) findViewById(R.id.txt_cancel);
        mInstallTxt = (TextView) findViewById(R.id.txt_install);
        mProgressTxt = (TextView) findViewById(R.id.txt_progress);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mCancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onCancelClick();
                }
                dismiss();
            }
        });
        mInstallTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onConfirmClick();
                }
                dismiss();
            }
        });
        setCancelable(false);
    }

    @Override
    public BaseDialog setMessage(String msg) {
        return this;
    }

    public void setProgress(int progress) {
        if(progress > 0) {
            mProgressBar.setProgress(progress);
            mProgressTxt.setText(progress + "%");
            if(progress == mProgressBar.getMax()) {
                mInstallTxt.setEnabled(true);
            } else {
                mInstallTxt.setEnabled(false);
            }
        }
    }

    public void setOnListener(OnLostener listener) {
        mListener = listener;
    }

    public interface OnLostener {
        void onConfirmClick();
        void onCancelClick();
    }

}
