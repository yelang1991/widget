package imwallet.dichen.com.library.utils;

import android.app.Activity;

import java.util.Stack;

import imwallet.dichen.com.library.framework.BaseActivity;

/**
 * 用于同一管理Activity
 *
 * @author Administrator
 * @date 2018/6/12
 */

public class ActivityManager {
    private Stack<BaseActivity> mActivityStack = new Stack<>();
    private int ACTIVITY_MAX_NUMBER = 20;
    private static ActivityManager sInstace;

    public static ActivityManager get() {
        if(sInstace == null) {
            synchronized (ActivityManager.class) {
                if(sInstace == null) {
                    sInstace = new ActivityManager();
                }
            }
        }
        return sInstace;
    }

    /**
     * activity 添加入栈中
     *
     * @param activity
     */
    public synchronized void put(BaseActivity activity) {
        if(mActivityStack.size() > ACTIVITY_MAX_NUMBER) {
            BaseActivity act = mActivityStack.get(mActivityStack.size() - 1);
            if(!act.isFinishing()) {
                act.finish();
            }
            remove(activity);
        }
        mActivityStack.push(activity);
    }

    /**
     * activity 从栈中移除
     *
     */
    public void remove(Activity activity) {
        mActivityStack.remove(activity);
    }

    /**
     * 清除所有的activity
     */
    public void clear() {
      for(BaseActivity activity : mActivityStack) {
          if(!activity.isFinishing()) {
              activity.finish();
          }
      }
      mActivityStack.clear();
    }

    /**
     * 获取当前正在显示的activity
     *
     * @return
     */
    public Activity getCurrentActivity() {
        return mActivityStack.get(mActivityStack.size() - 1);
    }
}
