package imwallet.dichen.com.library.utils;

import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;
import java.io.File;

import imwallet.dichen.com.library.app.App;

/**
 * 项目：  wallet-android
 * 类名：  DataCleanManager.java
 * 时间：  2018/6/21 11:19
 * 描述：  ${TODO}
 */
public class DataCleanManager {
    private static final String TAG = "DataCleanManager";

    /**
     * 清楚本应用内部缓存(/data/data/com.xxx.xxx/cache)
     */
    public static void cleanInternalCache() {
        deleteFiles(App.get().getCacheDir());
    }

    /**
     * 清除本应用所有数据库(/data/data/com.xxx.xxx/databases)
     */
    public static void cleanDatabases() {
        deleteFiles(new File("/data/data/"
                + App.get().getPackageName() + "/databases"));
    }

    /**
     * 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs)
     */
    public static void cleanSharedPreference() {
        deleteFiles(new File("/data/data/"
                + App.get().getPackageName() + "/shared_prefs"));
    }

    /**
     * 按名字清除本应用数据库
     *
     * @param dbName 数据库名
     */
    public static void cleanDatabaseByName(String dbName) {
        App.get().deleteDatabase(dbName);
    }

    /**
     * 清除/data/data/com.xxx.xxx/files下的内容
     */
    public static void cleanFiles() {
        deleteFiles(App.get().getFilesDir());
    }

    /**
     * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache)
     */
    public static void cleanExternalCache() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            deleteFiles(App.get().getExternalCacheDir());
        }
    }

    /**
     * 清除自定义路径下的文件，使用需小心，请不要误删。而且只支持目录下的文件删除
     *
     * @param filePath 文件路径
     */
    public static void cleanCustomCache(String filePath) {
        deleteFiles(new File(filePath));
    }

    /**
     * 清除本应用所有的数据
     *
     * @param filepath 文件路径
     */
    public static void cleanApplicationData(String... filepath) {
        cleanExternalCache();
        cleanDatabases();
        cleanSharedPreference();
        cleanFiles();
        for (String filePath : filepath) {
            cleanCustomCache(filePath);
        }
    }

    /**
     * 删除方法，判断directory是否文件,如果是文件则删除,如果是文件夹则遍历文件夹中的文件再删除
     *
     * @param directory 删除的文件
     */
    private static void deleteFiles(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                if (item.isFile()) {
                    Log.d(TAG, "deleteFiles: " + item.getAbsolutePath());
                    item.delete();
                } else if (item.isDirectory()) {
                    File[] files = item.listFiles();
                    for (File file : files) {
                        deleteFiles(file);
                    }
                }
            }
        } else if (directory != null && directory.exists() && directory.isFile()) {
            Log.d(TAG, "deleteFiles: " + directory.getAbsolutePath());
            directory.delete();
        }
    }

    /**
     * 获取内部缓存文件总大小
     *
     * @return 返回格式化后的字符串
     * @throws Exception
     */
    public static String getTotalCacheSize() throws Exception {
        long cacheSize = getFolderSize(App.get().getCacheDir());
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            cacheSize += getFolderSize(App.get().getExternalCacheDir());
        }
        return Formatter.formatFileSize(App.get(), cacheSize);
    }

    /**
     * 获取文件大小
     *
     * @param file 要大小获取的文件
     * @return 返回文件大小
     * @throws Exception
     */
    public static long getFolderSize(File file) throws Exception {
        long size = 0;
        try {
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // 如果下面还有文件
                if (fileList[i].isDirectory()) {
                    size = size + getFolderSize(fileList[i]);
                } else {
                    size = size + fileList[i].length();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }
}
