package imwallet.dichen.com.library.utils;


import android.content.Context;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import java.io.File;

import imwallet.dichen.com.library.R;
import imwallet.dichen.com.library.utils.image.CircleTransform;
import imwallet.dichen.com.library.utils.image.RoundTransform;

/**
 *
 * @author Administrator
 * @date 2018/5/16
 */

public class ImageLoader {
    private static ImageLoader sInstance;

    public static ImageLoader get() {
        if (sInstance == null) {
            synchronized (ImageLoader.class) {
                if (sInstance == null) {
                    sInstance = new ImageLoader();
                }
            }
        }
        return sInstance;
    }

    public void loadCircleImage(Context context, String url, ImageView imageView) {
        loadCircleImage(context, url, imageView, R.mipmap.img_circle);
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param imageView
     */
    public void loadCircleImage(Context context, String url, ImageView imageView, int errorRes) {
        if(context == null || imageView == null || TextUtils.isEmpty(url)) {
            return;
        }
        Glide.with(context)
                .load(url)
                .dontAnimate()
                .crossFade()
                .error(errorRes)
                .placeholder(errorRes)
                .bitmapTransform(new CenterCrop(context), new CircleTransform(context))
                .into(imageView);
    }

    /**
     * 加载本地图片
     *
     * @param context
     * @param res
     * @param imageView
     */
    public void loadCircleImage(Context context, @DrawableRes int res, ImageView imageView) {
        if(context == null || imageView == null) {
            return;
        }
        Glide.with(context)
                .load(res)
                .dontAnimate()
                .crossFade()
                .error(R.mipmap.img_circle)
                .placeholder(R.mipmap.img_circle)
                .bitmapTransform(new CenterCrop(context), new CircleTransform(context))
                .into(imageView);
    }

    /**
     * 加载图片文件
     *
     * @param context
     * @param file
     * @param imageView
     */
    public void loadCircleImage(Context context, File file, ImageView imageView) {
        if(context == null || imageView == null || file == null) {
            return;
        }
        Glide.with(context)
                .load(file)
                .dontAnimate()
                .crossFade()
                .error(R.mipmap.img_circle)
                .placeholder(R.mipmap.img_circle)
                .bitmapTransform(new CenterCrop(context), new CircleTransform(context))
                .into(imageView);
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param imageView
     */
    public void loadImage(Context context, String url, ImageView imageView) {
        loadRoundImage(context, url, imageView, 0);
    }

    public void loadRoundImage(Context context, String url, ImageView imageView, int radius) {
        if(context == null || imageView == null || TextUtils.isEmpty(url) || radius < 0) {
            return;
        }
        Glide.with(context)
                .load(url)
                .dontAnimate()
                .crossFade()
                .error(R.mipmap.img_default)
                .placeholder(R.mipmap.img_default)
                .bitmapTransform(new CenterCrop(context), new CircleTransform(context))
                .into(imageView);
    }

    /**
     * 加载本地图片
     *
     * @param context
     * @param res
     * @param imageView
     */
    public void loadImage(Context context, @DrawableRes int res, ImageView imageView) {
        loadRoundImage(context, res, imageView, 0);
    }

    public void loadRoundImage(Context context, @DrawableRes int res, ImageView imageView, int radius) {
        if(context == null || imageView == null || radius < 0) {
            return;
        }
        Glide.with(context)
                .load(res)
                .dontAnimate()
                .crossFade()
                .error(R.mipmap.img_default)
                .placeholder(R.mipmap.img_default)
                .bitmapTransform(new CenterCrop(context), new RoundTransform(context, radius))
                .into(imageView);
    }


    /**
     * 加载图片文件
     *
     * @param context
     * @param file
     * @param imageView
     */
    public void loadImage(Context context, File file, ImageView imageView) {
        loadRoundImage(context, file, imageView, 0);
    }

    public void loadRoundImage(Context context, File file, ImageView imageView, int radius) {
        if(context == null || imageView == null || file == null || radius < 0) {
            return;
        }
        Glide.with(context)
                .load(file)
                .dontAnimate()
                .crossFade()
                .error(R.mipmap.img_default)
                .placeholder(R.mipmap.img_default)
                .bitmapTransform(new CenterCrop(context), new RoundTransform(context, radius))
                .into(imageView);
    }
}
