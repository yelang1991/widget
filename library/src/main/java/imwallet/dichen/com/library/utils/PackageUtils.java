package imwallet.dichen.com.library.utils;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import imwallet.dichen.com.library.app.App;

/**
 * 项目：  wallet-android
 * 类名：  PackageUtils.java
 * 时间：  2018/6/21 11:30
 * 描述：  ${TODO}
 * @author Administrator
 */
public class PackageUtils {
    /**
     * 检查APP是否已经安装
     *
     * @param packageName 包名
     * @return 返回true表示已经存在
     */
    public static boolean checkApkExist(String packageName) {
        if (packageName == null || TextUtils.isEmpty(packageName.trim())) {
            return false;
        }
        try {
            ApplicationInfo info = App.get()
                    .getPackageManager()
                    .getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * 获取版本名
     */
    public static String getVersionName(String packageName) {
        String versionName = null;
        PackageInfo info = getPackageInfo(packageName);
        if (info != null) {
            versionName = info.versionName;
        }
        return versionName;
    }

    /**
     * 获取当前版本名
     *
     * @return 返回版本名
     */
    public static String getVersionName() {
        return getVersionName(App.get().getPackageName());
    }


    /**
     * 获取版本号
     *
     * @return 返回版本号
     */
    public static int getVersionCode(String packageName) {
        int versionCode = 0;
        PackageInfo info = getPackageInfo(packageName);
        if (info != null) {
            versionCode = info.versionCode;
        }
        return versionCode;
    }


    /**
     * 获取当前应用版本号
     *
     * @return 返回版本号
     */
    public static int getVersionCode() {
        return getVersionCode(App.get().getPackageName());
    }


    public static String getSystemVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * 获取应用名
     */
    public static String getAppName(String packageName) {
        PackageManager manager = App.get().getPackageManager();
        String appName = null;
        try {
            PackageInfo packInfo = manager.getPackageInfo(packageName, 0);
            if (packInfo != null) {
                ApplicationInfo appInfo = packInfo.applicationInfo;
                appName = appInfo.loadLabel(manager).toString();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appName;
    }

    /**
     * 获取应用图标
     */
    public static Drawable getAppIcon(String packageName) {
        PackageManager manager = App.get().getPackageManager();
        Drawable appIcon = null;
        try {
            PackageInfo packInfo = manager.getPackageInfo(packageName, 0);
            if (packInfo != null) {
                ApplicationInfo appInfo = packInfo.applicationInfo;
                appIcon = appInfo.loadIcon(manager);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appIcon;
    }

    /**
     * 获取包信息
     *
     * @param packageName 包名
     * @return 返回PackageInfo对象
     */
    public static PackageInfo getPackageInfo(String packageName) {
        PackageInfo pi = null;
        try {
            PackageManager manager = App.get().getPackageManager();
            pi = manager.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi;
    }

}
