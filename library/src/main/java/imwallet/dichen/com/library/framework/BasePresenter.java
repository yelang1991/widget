package imwallet.dichen.com.library.framework;

import io.reactivex.disposables.CompositeDisposable;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public abstract class BasePresenter<V extends BaseView> {
    protected V mView;
    protected CompositeDisposable mCompositeDisposable;

    public BasePresenter(V view) {
        mView = view;
    }

    protected void onCreate() {
        mCompositeDisposable = new CompositeDisposable();
    }

    protected void onDestory() {
        mCompositeDisposable.clear();
        mView = null;
    }
}
