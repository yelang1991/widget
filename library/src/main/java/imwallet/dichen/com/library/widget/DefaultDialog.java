package imwallet.dichen.com.library.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import imwallet.dichen.com.library.R;

/**
 * 默认dialog
 *
 * @author Administrator
 * @date 2018/5/30
 */

public class DefaultDialog extends BaseDialog {
    private TextView mMessageTxt;
    private TextView mCancelTxt;
    private TextView mConfirmTxt;
    private TextView mTitleTxt;

    public DefaultDialog(@NonNull Context context) {
        super(context);
        init();
    }

    public DefaultDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        init();
    }

    private void init() {
        getWindow().setBackgroundDrawableResource(R.drawable.dialog_default_shape);
        setContentView(R.layout.dialog_default);
        mMessageTxt = (TextView) findViewById(R.id.txt_msg);
        mCancelTxt = (TextView) findViewById(R.id.txt_cancel);
        mConfirmTxt = (TextView) findViewById(R.id.txt_confirm);
        mTitleTxt = (TextView) findViewById(R.id.txt_title);
    }

    @Override
    public DefaultDialog setMessage(String msg) {
        if(!TextUtils.isEmpty(msg)) {
            mMessageTxt.setText(msg);
        }
        return this;
    }

    @Override
    public void setTitle(@Nullable CharSequence title) {
        super.setTitle(title);
        if(!TextUtils.isEmpty(title)) {
            mTitleTxt.setVisibility(View.VISIBLE);
            mTitleTxt.setText(title);
        }
    }

    @Override
    public void setTitle(@StringRes int titleId) {
        super.setTitle(titleId);
        String title = getContext().getString(titleId);
        if(!TextUtils.isEmpty(title)) {
            mTitleTxt.setVisibility(View.VISIBLE);
            mTitleTxt.setText(title);
        }
    }

    public DefaultDialog setCancelText(String text) {
        if(!TextUtils.isEmpty(text)) {
            mCancelTxt.setText(text);
        }
        return this;
    }

    public DefaultDialog setConfirmText(String text) {
        if(!TextUtils.isEmpty(text)) {
            mConfirmTxt.setText(text);
        }
        return this;
    }

    @Override
    public void setOnClickListener(final OnClickListener clickListener) {
        if(clickListener != null) {
            mCancelTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onCancelClick(view);
                    dismiss();
                }
            });
            mConfirmTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onConfirmClick(view);
                    dismiss();
                }
            });
        }
    }
}
