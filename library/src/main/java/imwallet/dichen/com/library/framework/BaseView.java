package imwallet.dichen.com.library.framework;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public interface BaseView {
    /**
     * 请求出错
     *
     * @param code
     * @param msg
     */
    void requestError(int code, String msg);
}
