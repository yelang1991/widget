package imwallet.dichen.com.library.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.widget.TextView;

import imwallet.dichen.com.library.R;

/**
 * 加载弹窗
 *
 * @author Administrator
 * @date 2018/6/5
 */

public class LoadingDialog extends BaseDialog {
    private TextView mMsgTxt;

    public LoadingDialog(@NonNull Context context) {
        this(context, 0);
    }

    public LoadingDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.dialog_load);
        getWindow().setBackgroundDrawableResource(R.drawable.dialog_loading_shape);
        mMsgTxt = (TextView) findViewById(R.id.txt_message);
    }

    @Override
    public LoadingDialog setMessage(String msg) {
        if(!TextUtils.isEmpty(msg)) {
            mMsgTxt.setText(msg);
        }
        return this;
    }
}
