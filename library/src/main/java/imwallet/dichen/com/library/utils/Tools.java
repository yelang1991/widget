package imwallet.dichen.com.library.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import imwallet.dichen.com.library.app.App;

/**
 * 公共的工具类
 *
 * @author Administrator
 * @date 2018/5/16
 */

public class Tools {

    /**
     * 检查是否安装某个APP
     *
     * @param packageName 包名
     * @return
     */
    public static Boolean isInstall(String packageName) {
        //获取packagemanager
        final PackageManager packageManager = App.get().getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        //用于存储所有已安装程序的包名
        List<String> packageNames = new ArrayList<>();
        //从pinfo中将包名字逐一取出，压入pName list中
        if(packageInfos != null){
            for(int i = 0; i < packageInfos.size(); i++){
                String packName = packageInfos.get(i).packageName;
                packageNames.add(packName);
            }
        }
        //判断packageNames中是否有目标程序的包名，有TRUE，没有FALSE
        return packageNames.contains(packageName);
    }

    /**
     * 检查金额格式是否正确
     *
     * @param data 金额
     * @return
     */
    public static Boolean checkAmount(String data) {
        if(TextUtils.isEmpty(data)) {
            ToastUtils.showMessage("请输入数字");
            return false;
        }
        if(data.startsWith("0") || data.contains(".")) {
            ToastUtils.showMessage("数字格式错误~");
            return false;
        }
        return true;
    }

    /**
     * 弹出键盘
     *
     * @param view
     * 不要直接在oncreate中使用，因为view还没初始化完成
     */
    public static void showKeyBoard(View view) {
        view.requestFocus();
        InputMethodManager service = (InputMethodManager) App.get()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        service.showSoftInput(view, 0);
    }

    /**
     * 隐藏键盘
     *
     * @param view
     */
    public static void hideKeyBoard(View view) {
        view.clearFocus();
        InputMethodManager manager = (InputMethodManager) App.get()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 检查网络是否可用
     *
     * @return
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) App.get().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo networkinfo = manager.getActiveNetworkInfo();
        if (networkinfo == null || !networkinfo.isAvailable()) {
            return false;
        }
        return true;
    }

    /**
     * 获取系统语言环境
     */
    public static String getLanguage() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Locale locale = App.get().getResources().getConfiguration().locale;
            // 获取当前系统语言
            return locale.getLanguage();
        } else {
            Locale locale = Locale.getDefault();
            return locale.getLanguage() + "-" + locale.getCountry();
        }
    }

    /**
     * 获取状态栏高度
     *
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
