package imwallet.dichen.com.library.framework;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * 和Presenter绑定的Fragment基类
 *
 * @author Administrator
 * @date 2018/6/4
 */

public abstract class BasePresenterFragment<P extends BasePresenter<V>, V extends BaseView> extends BaseFragment {
    protected P mPresenter;

    /**
     * 初始化Presenter
     *
     * @return
     */
    public abstract P initPresenter();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        mPresenter = initPresenter();
        mPresenter.onCreate();
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestory();
        mPresenter = null;
    }
}
