package imwallet.dichen.com.zxing.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by Administrator on 2018/7/3.
 */

public class CodeView extends FrameLayout {

    public CodeView(@NonNull Context context) {
        this(context, null);
    }

    public CodeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
