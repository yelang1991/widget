package imwallet.dichen.com.zxing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 *
 * @author Administrator
 * @date 2018/7/3
 */

public class ScanActivity extends AppCompatActivity {
    private TextView mButton;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = (TextView) findViewById(R.id.btn_switch);
        mLinearLayout = (LinearLayout) findViewById(R.id.line1);
        TestLeakSingleton.getInstance(this).setTvAppName(mButton);
        mLinearLayout.removeView(mButton);
    }
}
