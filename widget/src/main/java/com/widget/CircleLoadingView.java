package com.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * 旋转圆圈的加载view
 *
 *
 * @author Administrator
 * @date 2018/6/25
 */

public class CircleLoadingView extends View {
    private int mInnerStart = 90;
    private int mOuterStart = -180;
    private Paint mInnerPaint;
    private Paint mOuterPaint;
    private int mInnerColor;
    private int mOutColor;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            postInvalidate();
        }
    };

    public CircleLoadingView(Context context) {
        this(context, null);
    }

    public CircleLoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    private void initPaint() {
        mInnerPaint = new Paint();
        mInnerPaint.setStyle(Paint.Style.STROKE);
        mInnerPaint.setStrokeWidth(15);
        mInnerColor = Color.BLACK;
        mInnerPaint.setColor(mInnerColor);
        mInnerPaint.setAntiAlias(true);

        mOuterPaint = new Paint();
        mOuterPaint.setStyle(Paint.Style.STROKE);
        mOuterPaint.setStrokeWidth(15);
        mOutColor = Color.BLACK;
        mOuterPaint.setColor(mOutColor);
        mOuterPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        RectF outerRectf = new RectF(15, 15, getWidth() - 15, getHeight() - 15);
        RectF innerRectf = new RectF(45, 45, getWidth() - 45, getHeight() - 45);
        canvas.drawArc(innerRectf, mInnerStart, -100, false, mInnerPaint);
        canvas.drawArc(outerRectf, mOuterStart, 260, false, mOuterPaint);

        mHandler.sendEmptyMessageDelayed(0, 200);
        mInnerStart -= 40;
        mOuterStart += 40;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width = 0, height = 0;
        if(widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else {
            int measureWidth = 150;
            if(widthMode == MeasureSpec.AT_MOST) {
                width = Math.min(measureWidth, widthSize);
            }
        }
        if(heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else {
            int measureHeight = 150;
            if(heightMode == MeasureSpec.AT_MOST) {
                height = Math.min(measureHeight, widthSize);
            }
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mHandler.removeCallbacksAndMessages(null);
    }
}
