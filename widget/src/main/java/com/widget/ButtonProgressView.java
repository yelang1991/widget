package com.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by chentao on 2018/6/25.
 */

public class ButtonProgressView extends AppCompatTextView {
    private Paint mBgPaint;
    private Paint mTextPaint;
    private Paint mOuterPaint;
    private Paint mButtonPaint;
    private Paint mProgressPaint;
    private int mProgress;

    public ButtonProgressView(Context context) {
        this(context, null);
    }

    public ButtonProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    private void initPaint() {
        mBgPaint = new Paint();
        mBgPaint.setStyle(Paint.Style.FILL);
        mBgPaint.setStrokeWidth(10);
        mBgPaint.setColor(Color.WHITE);
        //取消锯齿
        mBgPaint.setAntiAlias(true);

        mOuterPaint = new Paint();
        mOuterPaint.setStyle(Paint.Style.STROKE);
        mOuterPaint.setStrokeWidth(10);
        mOuterPaint.setColor(Color.parseColor("#e5e5e5"));
        //取消锯齿
        mOuterPaint.setAntiAlias(true);

        mProgressPaint = new Paint();
        mProgressPaint.setStyle(Paint.Style.STROKE);
        mProgressPaint.setStrokeWidth(10);
        mProgressPaint.setColor(Color.RED);
        //取消锯齿
        mProgressPaint.setAntiAlias(true);

        mTextPaint = new Paint();
        mTextPaint.setStyle(Paint.Style.STROKE);
        mTextPaint.setColor(Color.GREEN);
        mTextPaint.setTextSize(60);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        //取消锯齿
        mTextPaint.setAntiAlias(true);

        mButtonPaint = new Paint();
        mButtonPaint.setStyle(Paint.Style.FILL);
        mButtonPaint.setStrokeWidth(10);
        mButtonPaint.setColor(Color.BLUE);
        //取消锯齿
        mButtonPaint.setAntiAlias(true);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final int size = Math.min(getWidth(), getHeight());
        int width = getWidth()/2 - size/2;
        int height = getHeight()/2 - size/2;
        RectF rectf;
        if(getWidth() > getHeight()) {
            rectf = new RectF( width + 10, 10, width + size - 10, size - 10);
        } else {
            rectf = new RectF(10 , height + 10, size - 10, height + size - 10);
        }
        canvas.drawArc(rectf, -90, 360, true, mBgPaint);
        canvas.drawArc(rectf, -90, 360, false, mOuterPaint);
        Paint.FontMetrics metrics = mTextPaint.getFontMetrics();
        float top = metrics.top;
        float bottom = metrics.bottom;
        final float baseLineY = (rectf.centerY() - (top + bottom)/2);
        if(mProgress < 100) {
            canvas.drawText(mProgress + "%", rectf.centerX(), baseLineY, mTextPaint);
            float sweepAngle = 360 * mProgress / 100;
            canvas.drawArc(rectf, -90, sweepAngle, false, mProgressPaint);
        } else {
            final RectF rectF = new RectF(10, 10 ,getWidth() - 10, getHeight() - 10);
            canvas.drawRoundRect(rectF, size/2, size/2, mButtonPaint);
            canvas.drawText("完成", rectF.centerX(), baseLineY, mTextPaint);
        }
    }

    public synchronized void setProgress(int progress) {
        if(progress > 100 || progress < 0) {
            return;
        }
        mProgress = progress;
        postInvalidate();
    }
}
