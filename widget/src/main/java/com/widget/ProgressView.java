package com.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * 自定义圆形进度
 *
 * paint文字居中 https://blog.csdn.net/zly921112/article/details/50401976
 * Created by chentao on 2018/6/24.
 */
public class ProgressView extends AppCompatTextView {
    /**
     * 背景颜色
     */
    private int mBackgroundColor;
    /**
     * 虚线间隔
     */
    private float mStrokeInterval;
    /**
     * 进度条颜色
     */
    private int mStrokeColor;
    /**
     * 进度条粗细
     */
    private float mStrokeWidth;
    /**
     * 最大进度
     */
    private int mMaxProgress;
    /**
     * 文字颜色
     */
    private int mTextColor;
    /**
     * 文字大小
     */
    private float mTextSize;
    /**
     * 文字描述
     */
    private String mText = "0%";
    /**
     * 样式
     */
    private int mSytle;
    private int mProgress;
    private Paint mPaint;
    private Paint mBgPaint;
    private Paint mTextPaint;
    private Context mContext;
    /**
     * 充满圆
     */
    private final int STYLE_FILL = 1;
    /**
     * 实线带边框
     */
    private final int STYLE_STROKE = 2;
    /**
     * 彩虹边框
     */
    private final int STYLE_RAINBOW = 3;
    /**
     * 虚线边框
     */
    private final int STYLE_DOTTED_LINE = 4;

    public ProgressView(Context context) {
        this(context, null);
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initAttrs(attrs);
    }

    private void initAttrs(AttributeSet attrs) {
        TypedArray array = mContext.obtainStyledAttributes(attrs, R.styleable.ProgressView);
        mBackgroundColor = array.getColor(R.styleable.ProgressView_background_color,
                Color.parseColor("#e5e5e5"));
        mStrokeInterval = array.getDimension(R.styleable.ProgressView_stroke_interval, 10);
        mStrokeColor = array.getColor(R.styleable.ProgressView_stroke_color,
                Color.parseColor("#3C7FFF"));
        mStrokeWidth = array.getDimension(R.styleable.ProgressView_stroke_width, 15);
        mMaxProgress = array.getInteger(R.styleable.ProgressView_max_progress, 100);
        mTextColor = array.getColor(R.styleable.ProgressView_text_color, Color.RED);
        mTextSize = array.getDimension(R.styleable.ProgressView_text_size, 50);
        mSytle = array.getInt(R.styleable.ProgressView_style, STYLE_STROKE);
        array.recycle();
    }

    private void initPaint() {
        mBgPaint = new Paint();
        mBgPaint.setStyle(Paint.Style.FILL);
        mBgPaint.setStrokeWidth(10);
        mBgPaint.setColor(mBackgroundColor);
        //取消锯齿
        mBgPaint.setAntiAlias(true);
        mPaint = new Paint();
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setColor(mStrokeColor);
        if(mSytle == STYLE_FILL) {
           mPaint.setStyle(Paint.Style.FILL);
        } else if(mSytle == STYLE_STROKE) {
            mPaint.setStyle(Paint.Style.STROKE);
        } else if(mSytle == STYLE_DOTTED_LINE) {
            mPaint.setStyle(Paint.Style.STROKE);
            // 绘制虚线
            mPaint.setPathEffect(new DashPathEffect(new float[]{mStrokeInterval, mStrokeInterval}, 0));
        } else {
            mPaint.setStyle(Paint.Style.STROKE);
            // 绘制虚线
            mPaint.setPathEffect(new DashPathEffect(new float[]{mStrokeInterval * 2, mStrokeInterval}, 0));
            int[] colors = {Color.GREEN, Color.YELLOW, Color.RED, Color.BLUE};
            Shader shader = new SweepGradient(getMeasuredWidth()/2, getMeasuredHeight()/2, colors, null);
            mPaint.setShader(shader);
        }
        //取消锯齿
        mPaint.setAntiAlias(true);

        mTextPaint = new Paint();
        mTextPaint.setColor(mTextColor);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        //取消锯齿
        mTextPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        RectF rectF = new RectF(10, 10, getWidth() - 10 , getHeight() - 10);
        int sweepAngle = mProgress*360/100;
        canvas.drawArc(rectF, -90, 360, true, mBgPaint);
        if(mSytle == STYLE_FILL) {
            canvas.drawArc(rectF, -90, sweepAngle, true, mPaint);
        } else {
            canvas.drawArc(rectF, -90, sweepAngle, false, mPaint);
        }
        Paint.FontMetrics metrics = mTextPaint.getFontMetrics();
        float top = metrics.top;
        float bottom = metrics.bottom;
        float baseLineY = (rectF.centerY() - (top + bottom)/2);
        canvas.drawText(mText, rectF.centerX(), baseLineY, mTextPaint);
    }

    public synchronized void setProgress(int progress) {
        if(progress > mMaxProgress || progress < 0) {
            return;
        }
        mProgress = progress;
        mText = progress + "%";
        postInvalidate();
    }

    public synchronized void setText(String text) {
        mText = text;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mPaint.reset();
        mBgPaint.reset();
        mTextPaint.reset();
        mPaint = null;
        mBgPaint = null;
        mTextPaint = null;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        initPaint();
    }
}
