package com.application;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.search.SearchFragment;

import imwallet.dichen.com.library.framework.BaseFragment;

/**
 *
 * @author Administrator
 * @date 2018/7/2
 */

public class HomeFragment extends BaseFragment {
    private TextView mTestTxt;

    @Override
    public View createView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTestTxt = (TextView) view.findViewById(R.id.txt_test);
        mTestTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToFragment(SearchFragment.class);
            }
        });
    }

    @Override
    public boolean onBack() {
        ToastUtils.showMessage("后退键");
        pop();
        return true;
    }
}
