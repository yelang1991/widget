package com.application.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.application.R;
import com.application.bean.Region;
import com.application.search.SearchFragment;

import java.util.List;

import imwallet.dichen.com.library.bean.User;
import imwallet.dichen.com.library.framework.BasePresenterActivity;
import imwallet.dichen.com.library.utils.ToastUtils;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public class LoginActivity extends BasePresenterActivity<LoginPresenter, LoginView> implements LoginView {
    private TextView mLoginTxt;
    private TextView mRegionTxt;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mLoginTxt = (TextView) findViewById(R.id.txt_login);
        mRegionTxt = (TextView) findViewById(R.id.txt_region);
        mLoginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.Login();
            }
        });
        mRegionTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.getRegion();
            }
        });
    }

    @Override
    public LoginPresenter initPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    public void requestError(int code, String msg) {
        ToastUtils.showMessage(msg);
    }

    @Override
    public void loginSuccess(User user) {
        ToastUtils.showMessage(user.getMobile() + "登录成功~");
        Bundle bundle = new Bundle();
        bundle.putString("data", "测试数据");
        pushFragment(SearchFragment.class, bundle, true);
    }

    @Override
    public void getRegionSuccess(List<Region> data) {
        ToastUtils.showMessage(data.get(0).getChineseName());
    }
}
