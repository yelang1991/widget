package com.application.login;

import com.application.bean.Region;

import java.util.List;

import imwallet.dichen.com.library.bean.User;
import imwallet.dichen.com.library.framework.BaseView;

/**
 * Created by Administrator on 2018/7/9.
 */

interface LoginView extends BaseView {
    void loginSuccess(User user);

    void getRegionSuccess(List<Region> data);
}
