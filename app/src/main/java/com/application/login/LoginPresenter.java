package com.application.login;

import android.util.Log;

import com.application.bean.Region;
import com.application.service.AppService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import imwallet.dichen.com.library.bean.User;
import imwallet.dichen.com.library.framework.BasePresenter;
import imwallet.dichen.com.library.http.RequestObservable;
import imwallet.dichen.com.library.http.ApiManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    public LoginPresenter(LoginView view) {
        super(view);
    }

    public void Login() {
        String pwd = "fKM5l/gwslt5IyKhKKCuQwkXmXyYL1n71kEeN/tc2YADj+zE9FSLx64zRBx/Jecd3SHd0TszRIbKI9JqSkp5IAEsZrWNV/aNr+H1zeMkS2gj5GYiSwOWoC8nXD4/ggLAwF4isRq4bQtxew65/QREzKNmtZrw8L47FL4QIOFNGd4=";
        Map<String, String> params = new HashMap<>(3);
        params.put("mobile", "15820762583");
        params.put("password", pwd);
        params.put("regionCode", "86");
        ApiManager.get()
                .createService(AppService.class)
                .login(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RequestObservable<User>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(User data) {
                        mView.loginSuccess(data);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        mView.requestError(code, msg);
                    }
                });

    }

    public void getRegion() {
        ApiManager.get()
                .createService(AppService.class)
                .getRegion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RequestObservable<List<Region>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Region> data) {
                        mView.getRegionSuccess(data);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        mView.requestError(code, msg);
                    }
                });

    }

    @Override
    protected void onDestory() {
        super.onDestory();
        Log.e("LoginPresenter", "onDestory");
    }
}
