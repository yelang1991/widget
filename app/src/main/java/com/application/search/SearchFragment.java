package com.application.search;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.application.HomeFragment;
import com.application.R;

import java.util.List;

import imwallet.dichen.com.library.bean.Book;
import imwallet.dichen.com.library.framework.BasePresenterFragment;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public class SearchFragment extends BasePresenterFragment<SearchPresenter, SearchView> implements SearchView {
    private TextView mSearchTxt;
    private TextView mContentTxt;

    @Override
    public View createView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_search, container, false);
    }

    @Override
    public SearchPresenter initPresenter() {
        return new SearchPresenter(this);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        mSearchTxt = (TextView) view.findViewById(R.id.txt_search);
        mContentTxt = (TextView) view.findViewById(R.id.txt_content);
        mSearchTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushFragment(HomeFragment.class);
            }
        });
    }

    @Override
    protected void initData() {
        super.initData();
        String data = getArguments().getString("data");
        mSearchTxt.setText(data);
    }

    @Override
    public void searchSuccess(List<Book> bookList) {
        mContentTxt.setText(bookList.get(0).getTitle());
    }

    @Override
    public void onDestory() {
        Toast.makeText(getActivity(), "onDestory", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestError(int code, String msg) {

    }
}
