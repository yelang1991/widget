package com.application.search;


import android.util.Log;

import imwallet.dichen.com.library.http.Response;
import com.application.service.AppService;

import imwallet.dichen.com.library.framework.BasePresenter;
import imwallet.dichen.com.library.http.ApiManager;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public class SearchPresenter extends BasePresenter<SearchView> {

    public SearchPresenter(SearchView view) {
        super(view);
    }

    public void search(String name) {
        ApiManager.get().createService(AppService.class)
        .searchBook(name,  null, 0, 1)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onNext(@NonNull Response response) {

            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    protected void onDestory() {
        mView.onDestory();
        super.onDestory();
        Log.e("SearchPresenter", "onDestory");
        mCompositeDisposable.clear();
        mCompositeDisposable.dispose();
    }
}
