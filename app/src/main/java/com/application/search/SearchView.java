package com.application.search;

import java.util.List;

import imwallet.dichen.com.library.framework.BaseView;
import imwallet.dichen.com.library.bean.Book;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public interface SearchView extends BaseView {

    void searchSuccess(List<Book> bookList);
    void onDestory();
}
