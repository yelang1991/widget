package com.application.service;

import com.application.bean.Region;

import java.util.List;
import java.util.Map;

import imwallet.dichen.com.library.http.Response;

import imwallet.dichen.com.library.bean.User;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 *
 * @author Administrator
 * @date 2018/7/9
 */

public interface AppService {
    /**
     * 搜索书籍
     *
     * @param name
     * @param tag
     * @param start
     * @param count
     * @return
     */
    @GET("book/search")
    Observable<Response> searchBook(@Query("q") String name,
                                    @Query("tag") String tag,
                                    @Query("start") int start,
                                    @Query("count") int count);

    /**
     * 登录
     *
     * @return
     */
    @POST("login")
    Observable<Response<User>> login(@Body Map<String, String> params);

    /**
     * 获取国家与地区
     *
     * @return
     */
    @GET("information/country_region")
    Observable<Response<List<Region>>> getRegion();
}
