package com.application;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.application.search.SearchFragment;

import imwallet.dichen.com.library.framework.BaseActivity;

/**
 *
 * @author Administrator
 * @date 2018/6/28
 */

public class SecondActivity extends BaseActivity {
    private TextView mTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        mTextView = (TextView) findViewById(R.id.txt_test);
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecondActivity.this, SearchFragment.class);
                startActivity(intent);
            }
        });
    }

}
