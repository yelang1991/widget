package com.application;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import imwallet.dichen.com.library.framework.BaseActivity;

import org.greenrobot.eventbus.EventBus;

/**
 *
 * @author Administrator
 * @date 2018/6/25
 */

public class TestActivity extends BaseActivity {
    private TextView mTestTxt;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mTestTxt = (TextView) findViewById(R.id.txt_test);
        final String text = mTestTxt.getText().toString();
        SpannableString spannableString = new SpannableString(text);
        ClickableSpan startClickspan = new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(Color.BLACK);
            }

            @Override
            public void onClick(View widget) {
                ToastUtils.showMessage("startClick");
            }
        };
        ClickableSpan endClickableSpan = new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(Color.parseColor("#3C7FFF"));
            }

            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(TestActivity.this, SecondActivity.class);
                startActivity(intent);
                Event event = new Event();
                event.setMsg("测试一条消息");
                EventBus.getDefault().postSticky("测试一条消息");
            }
        };
        spannableString.setSpan(startClickspan, 0,
                text.indexOf(","), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(endClickableSpan,
                text.indexOf(",") , text.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        mTestTxt.setHighlightColor(Color.TRANSPARENT);
        mTestTxt.setMovementMethod(LinkMovementMethod.getInstance());
        mTestTxt.setText(spannableString);
    }

    public void click(View view) {
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.txt1:
                bundle.putString(TestFragment.KEY_DATA, "text1");
                pushFragment(TestFragment.class, bundle);
                break;
            case R.id.txt2:
                bundle.putString(TestFragment.KEY_DATA, "text2");
                pushFragment(TestFragment.class, bundle);
                break;
            case R.id.txt3:
                bundle.putString(TestFragment.KEY_DATA, "text3");
                pushFragment(TestFragment.class, bundle);
                break;
            case R.id.txt4:
                bundle.putString(TestFragment.KEY_DATA, "text4");
                pushFragment(TestFragment.class, bundle);
                break;
            default:

                break;
        }
    }

}
