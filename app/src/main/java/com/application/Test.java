package com.application;

import com.application.utils.Base64Utils;
import com.application.utils.RSAHelper;

import org.apache.commons.codec.binary.Base64;

import java.net.URLDecoder;
import java.util.Map;

/**
 *
 * @author Administrator
 * @date 2018/6/26
 */

public class Test {
    String privateKey2 = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKdg0SlM6sXWDLUkpipA2nAUsS8dLI/XXyIMpaGPywh/Mfrde1FJ9q9rlw1MC4uywF8wV9BTFl/v9pTQlhDeRp8+N7VpTM61h/9ZZS6Re/6r0wpnbExtygkkC7wbV2tPGgmZxVC4mSQ98yagBeIGhA41OgWLzeQ4CaH+ttwaAshtAgMBAAECgYAfT7lRJJZ21Agkiuqa1O8IMJbE3YxMjFm7oOoLQz8UI9dRusUq+88uGc50KlmulI3wQLbsvd5drCJQzISGO06yhdrecwmspDoAqfusEC9P32DspCUBNZ8VfiQUbBwrtzzFzGOc7ChtIU49rPX4BttodbZ+0iblPs/IfNYP7rVKgQJBAOBAyEgp185A67lPUc+g2qauHuSMSp6oiARXY7HDD8Bho7LSDQ99Yje/ANbyOK1z75F2ZVaaXhIXza9M5bhQUqkCQQC/EtEb9J2TsuMZMAvIt88al9kdG92BR9w4+rPIiJWeaZ0WR1hhFTdWu8+wcdsG+JSNPA5HSkFKAWEeP67z6+YlAkEAmt+CsnvbY8FoSsGiAZ/Ygdas1TCidDJ2RXFJE8JgRX/pOx1gEbuJMnDCFrCQPhJceHxo/kd8AkjYL2IPIJywiQJAUpcnnBXYJwej8jc6YMBS3QPV7ScmT8NoBZkYLQ4c3iR4qjCcmxo4mV0FIReVvSWl0IatBVOrdPmzqQeN8BF/oQJAGWCF4aP1hB6t8jeNlJk+gwvyg8XgKCf8sssplh73HnMdAx2XPWHnLNlAng1t7MNbBk5IYLs9BzKuk2mJOpBClA==";
    static String publicKey;
    static String privateKey;

    static {
        try {
            Map<String, Object> keyMap = RSAHelper.genKeyPair();
            publicKey = RSAHelper.getPublicKey(keyMap);
            privateKey = RSAHelper.getPrivateKey(keyMap);
            System.err.println("公钥: \n\r" + publicKey);
            System.err.println("私钥： \n\r" + privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        test();
    }

    //加密数据
    static void test() throws Exception {
        String source = new String("12345678");
        source = URLDecoder.decode(source, "utf-8");
        System.out.println("\r加密前文字：\r\n" + source);
        byte[] data = source.getBytes("UTF-8");
        byte[] encodedData = RSAHelper.encryptByPublicKey(data, publicKey);
        System.out.println("加密后文字：\r\n" + new String(Base64Utils.encode(encodedData)));
        byte[] decodedData = RSAHelper.decryptByPrivateKey(encodedData, privateKey);
        String target = new String(decodedData);
        System.out.print("解密后文字: " + target);
    }

    //验证签名
    static void testSign() throws Exception {
        System.err.println("私钥加密——公钥解密");
        String source = "这是一行测试RSA数字签名的无意义文字";
        System.out.println("原文字：\r\n" + source);
        byte[] data = source.getBytes();
        String encodedData = RSAHelper.encryptByPrivateKey(data, privateKey);
        System.out.println("加密后：\r\n" + new String(encodedData));
        byte[] decodedData = RSAHelper.decryptByPublicKey(encodedData.getBytes(), publicKey);
        String target = new String(decodedData);
        System.out.println("解密后: \r\n" + target);
        System.err.println("私钥签名——公钥验证签名");
        String sign = RSAHelper.sign(encodedData.getBytes(), privateKey);
        System.err.println("签名:\r" + sign);
        boolean status = RSAHelper.verify(encodedData.getBytes(), publicKey, sign);
        System.err.println("验证结果:\r" + status);
    }
}

