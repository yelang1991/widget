package com.application;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import imwallet.dichen.com.library.framework.BaseFragment;

/**
 *
 * @author Administrator
 * @date 2018/7/2
 */

public class TestFragment extends BaseFragment {
    private TextView mTextView;
    public static String KEY_DATA = "data";

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextView = (TextView) view.findViewById(R.id.txt_test);
    }

    @Override
    public View createView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String data = getArguments().getString(KEY_DATA);
        mTextView.setText(data);
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushFragment(HomeFragment.class, null, true);
            }
        });
    }

    @Override
    public boolean onBack() {
        ToastUtils.showMessage("测试");
        pop();
        return true;
    }
}
