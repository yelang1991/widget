package com.application;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;

import imwallet.dichen.com.library.app.App;

/**
 *
 * @author Administrator
 * @date 2018/5/15
 */

public class ToastUtils {
    private static Toast sToast;

    public static void showMessage(final String msg) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(TextUtils.isEmpty(msg)) {
                    return;
                }
                if(sToast == null) {
                    sToast = Toast.makeText(App.get(), "", Toast.LENGTH_SHORT);
                    sToast.setGravity(Gravity.CENTER, 0, 0);
                }
                sToast.setText(msg);
                sToast.show();
            }
        });
    }
}
